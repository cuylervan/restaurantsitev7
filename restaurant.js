function validate(){
	var name = (document.getElementById("myname"));
	var email = (document.getElementById("myemail"));
	var phone = (document.getElementById("myphone"));
	var reason = (document.getElementById("myreason").selectedIndex);
	var info = (document.getElementById("myinfo"));
	var been = (document.getElementById("mybeen").checked);
	var notbeen = (document.getElementById("mynotbeen").checked);
	var Mday = (document.getElementById("myM"));
	var Tday = (document.getElementById("myT"));
	var Wday = (document.getElementById("myW"));
	var THday = (document.getElementById("myTH"));
	var Fday = (document.getElementById("myF"));
	
	if (name === null || name === ""){
		alert("Please enter your name.");
	}
	if (email === null || email === ""){
		alert("Please enter your email.");
	}
	if (phone === null || phone === ""){
		alert("Please enter your Phone number.");
	}
	if (reason === "other" && info === ""){
		alert("Please explain your reason for inquiry in the drop down menu.");
	}
	if (been === false && notbeen === false){
		alert("Please indicate if you have visited us before.");
	}
			
	if (Mday.checked === false &&
		Tday.checked === false &&
		Wday.checked === false &&
		THday.checked === false &&
		Fday.checked === false){
			alert("Please indicate a day we can reach you.");
		}
}